import ROOT
import os
import numpy as np
import matplotlib.pyplot as plt
import mplhep as hep
hep.style.use("CMS") 
    
def defineEventPreSelection(df):
    
    """ 
    define event pre-selection 
    among others only two high purity tracks are allowed, and the ZDC needs to be available
    a mask is added to the dataframe called `event_preselection`
    """
    
    df = df.Define('clusterCompatibility', 'filter.pclusterCompatibilityFilter>0') \
           .Define('primaryVertex', 'filter.pprimaryVertexFilter>0') \
           .Define('hfFilter', 'filter.phfPosFilterTh7p3==0 && filter.phfNegFilterTh7p6==0') \
           .Define('fireTrigger', 'hlt.HLT_HIUPC_SingleMuOpen_NotMBHF2AND_v1>0') \
           .Define('twoHighPurityTracks', 'hi.hiNtracksHighPurity==2') \
           .Define('runsWithZDC','run>=326776') \
           .Define('event_preselection', 'clusterCompatibility && primaryVertex && hfFilter && fireTrigger && twoHighPurityTracks')
    
    return df


def addZDCInfo(df):
    
    """adds the ZDC info columns: energy deposites in the + and - sides"""
    
    df =df.Define('zdcPlus', 'hi.hiZDCplus') \
          .Define('zdcMinus', 'hi.hiZDCminus')
    
    return df

def addAdditionalMuonInfo(df, muon_mask):
    
    """
    skim the muon and dimuon
    df = input RDataFrame
    muon_mask = mask used to select single muons
    """
    
    #skim single muon information
    rec_muon_labels = ["charge", "pt", "eta", "phi", "fireL1Trigger", "global_nMuonHits", "global_normChi2", \
                       "isGlobal", "isHybridSoft", "isPF", "isSoft", "isTight", "isTracker", "nMuonStations", \
                       "oneStationTight", "track_dXY", "track_dZ", "track_isHighPurity", "track_nPixelHits", \
                       "track_nPixelLayers", "track_nTrackerLayers"]
    for v in rec_muon_labels:
        df = df.Redefine(f'rec_muon_{v}', f'rec_muon_{v}[{muon_mask}]')
    
    #skim double muon information
    dimuon_mask = muon_mask.replace('rec_muon', 'rec_dimuon')
    df = df.Define(dimuon_mask, f'dimuon_mask(rec_dimuon_muon1Idx, rec_dimuon_muon2Idx, {muon_mask})')
    for v in ["pt", "eta", "phi", "rapidity", "charge", "mass", \
              "muon1Idx", "muon2Idx", "vertex_normChi2", "vertex_prob"]:
        df = df.Redefine(f'rec_dimuon_{v}', f'rec_dimuon_{v}[{dimuon_mask}]') \
               .Redefine(f'rec_dimuon_{v}', f'rec_dimuon_{v}.size()>0 ? rec_dimuon_{v}[0] : -1')

    return df

def defineMuonSelection(df):

    """
    define muon selection based on the variables added by the `addAdditionalMuonInfo` method
    """
    
    df = df.Define('rec_muon_acc_pid', 'muon_acceptance_softid(rec_muon_pt, rec_muon_eta)') \
           .Define('rec_muon_softid', 'rec_muon_acc_pid && \
                                       rec_muon_isTracker>0 && rec_muon_oneStationTight>0 && \
                                       rec_muon_track_isHighPurity>0 && rec_muon_track_nPixelLayers>0 && \
                                       rec_muon_track_nTrackerLayers>5 && \
                                       abs(rec_muon_track_dXY)<0.3 && abs(rec_muon_track_dZ)<20.') \
           .Define('rec_muon_acc_trigger', 'muon_acceptance_trigger(rec_muon_pt, rec_muon_eta)') \
           .Define('rec_muon_trigger', 'rec_muon_acc_trigger>0 && rec_muon_fireL1Trigger>0') \
           .Define('nmu', 'rec_muon_pt.size()') \
           .Define('ngood_mu', 'Sum(rec_muon_softid>0)')
    
    return df


def defineRecDimuonInfo(df):
    
    """
    based on the variables defined with the `defineMuonSelection` and `addAdditionalMuonInfo`
    this method defines the final dimuon variables including a cut on the di-muon mass
    """
    
    df = df.Redefine('rec_dimuon_pt','dimuon_pt(rec_muon_pt, rec_muon_phi)') \
           .Redefine('rec_dimuon_phi','dimuon_phi(rec_muon_pt, rec_muon_phi)') \
           .Redefine('rec_dimuon_rapidity','dimuon_rapidity(rec_muon_pt, rec_muon_eta, rec_muon_phi)') \
           .Redefine('rec_dimuon_mass','dimuon_mass(rec_muon_pt, rec_muon_eta, rec_muon_phi)') \
           .Redefine('rec_dimuon_charge','rec_muon_charge.size()>1 ? (rec_muon_charge[0] + rec_muon_charge[1]) : -9') \
           .Define('rec_dimuon_softid', 'rec_muon_softid.size()>1 ? (rec_muon_softid[0] && rec_muon_softid[1]) : 0') \
           .Define('rec_dimuon_trigger', 'rec_muon_trigger.size()>1 ? (rec_muon_trigger[0] || rec_muon_trigger[1]) : 0') \
           .Define('rec_dimuon_acc_pid', 'rec_muon_acc_pid.size()>1 ? (rec_muon_acc_pid[0] && rec_muon_acc_pid[1]) : 0') \
           .Define('rec_dimuon_selection', 'rec_dimuon_vertex_prob>1e-6 && rec_dimuon_acc_pid && \
                                            abs(rec_dimuon_mass - 3.096)<1.2')
    return df


def defineGenDimuonInfo(df):
    """
    define generated dimuon information
    """
    df = df.Define('gen_dimuon_pt','dimuon_pt(gen_muon_pt, gen_muon_phi)') \
           .Define('gen_dimuon_rapidity','dimuon_rapidity(gen_muon_pt, gen_muon_eta, gen_muon_phi)') \
           .Define('gen_dimuon_absrap','abs(gen_dimuon_rapidity)') \
           .Define('gen_dimuon_mass','dimuon_mass(gen_muon_pt, gen_muon_eta, gen_muon_phi)') \
           .Define('gen_dimuon_charge','gen_muon_charge.size()>1 ? (gen_muon_charge[0] + gen_muon_charge[1]) : -9') \
           .Define('gen_muon_acc', 'gen_muon_pt>0.7 && abs(gen_muon_eta)>1.28 && abs(gen_muon_eta)<2.42') \
           .Define('gen_dimuon_acc', 'gen_muon_acc.size()>1 ? (gen_muon_acc[0] && gen_muon_acc[1]) : false')
    return df


def runJPsiSelection(file_list,
                     outname,
                     mu_attrs,**kwargs):
    
    """
    runs a selection of events and saves a skimmed RDataFrame
    file_list = array of files to process
    outname = output name of the skimmed file
    mu_attrs = array of muon attributes to save in summary
    """
    
    #start a RDataFrame to analyse the events
    alldf, tchains = helpers.openHiForest(file_list)
    
    #define event selection
    df = helpers.defineEventPreSelection(alldf)

    #define a pre-selection on empty bunches to monitor ZDC noise
    emptydf = df.Filter('runsWithZDC && clusterCompatibility && hfFilter && hlt.HLT_HIL1NotBptxOR_v1>0')
    emptydf = helpers.addZDCInfo(emptydf)
    
    #define pre-selection branch for all events which triggered and have the ZDC available
    lumidf = df.Filter('runsWithZDC && fireTrigger')

    #add the ZDC info 
    lumidf = helpers.addZDCInfo(lumidf)
    
    #define a branch all events passing pre-selection with two high purity tracks
    preseldf = lumidf.Filter('event_preselection')
    
    #add a flag if the event has OS muons
    preseldf =  preseldf.Define('has_dimuon_OS', 'Sum(rec_dimuon_charge==0)>0')
    
    #define a ZDC selection branch for events with OS di-muons passing the pre-selection
    #the muons do not necessarily need to be high purity at this stage
    zdcdf = preseldf.Filter('has_dimuon_OS') 
    
    #define the di-muon selection branch with two muons, at least one being a good muon 
    #the di-muon system will also be required to be in the vicinty of the J/Psi mass
    #additional selection variables are added by the methods being called
    seldf = preseldf.Define('rec_muon_maskHP', 'rec_muon_track_isHighPurity>0')
    seldf = helpers.addAdditionalMuonInfo(seldf, 'rec_muon_maskHP')
    seldf = helpers.defineMuonSelection(seldf)
    seldf = seldf.Filter('nmu==2 && ngood_mu>0')
    seldf = helpers.defineRecDimuonInfo(seldf)
    seldf = seldf.Filter('rec_dimuon_selection && rec_dimuon_trigger')
    
    #
    # with all the nodes defined it's time to execute them - this will be done by calling
    # the snapshot method which will run the selections and store the resulting events in files
    # we start by calling the most complicated branch which is the `seldf` one
    #
    
    #define collections for the selected muon and add to output before calling Snapshot
    columns = []
    for i in range(2):
        for attr in mu_attrs:
            seldf = seldf.Define(f'rec_muon{i+1}_{attr}', f'rec_muon_{attr}[{i}]') 
            columns.append(f'rec_muon{i+1}_{attr}')
    columns += ['rec_dimuon_mass', 'rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_charge', 'rec_dimuon_softid']
    print(f'Saving skimmed dataframe with columns={columns}')
    seldf.Snapshot('Events', outname, columns)

    #snapshot for luminosity computations
    lumi_outname = outname.replace('.root', '_lumi.root')
    print(f'Saving skimmed dataframe for computing luminosity in {lumi_outname}')
    lumidf.Snapshot('Lumi', lumi_outname, ['run', 'lumi'])
  
    #save snapshot for ZDC analysis
    zdc_fname = outname.replace('.root', '_zdc.root')
    print(f'Saving skimmed dataframe for analyzing ZDC spectra in {zdc_fname}')
    zdcdf.Snapshot('ZDC', zdc_fname, ['zdcPlus', 'zdcMinus'])

    #save snapshot for ZDC noise control
    empty_fname = outname.replace('.root','_emptybx.root')
    print(f'Saving skimmed dataframe for analyzing ZDC noise (empty BXs) in {empty_fname}')
    emptydf.Snapshot('ZDC', empty_fname, ['zdcPlus', 'zdcMinus'])



def runGenLevelSelection(file_list, outname, 
                         columns=['gen_dimuon_pt','gen_dimuon_rapidity', 'gen_dimuon_acc']):
    
    """
    runs a selection of gen level variables (no pre-selection applied)
    file_list = array of files to process
    outname = output name of the skimmed file
    """
    #start a RDataFrame to analyse the events
    df, tchains = helpers.openHiForest(file_list,addRecoTrees=False)
    
    #select opposite charge generated dimuons
    df = helpers.defineGenDimuonInfo(df).Filter('gen_dimuon_charge==0')
                
    #save snapshot
    print(f'Saving skimmed dataframe with columns={columns}')
    df.Snapshot('Events', outname, columns)
    

def processRecSimulation(file_list, outname, **kwargs):
    
    """
    runs on reconstructed simulation events, perform reco-gen matching, select events and saves a skimmed RDataFrame
    file_list = list of files to process
    outname = output name of the skimmed file
    """
    #start a RDataFrame to analyse the events
    df, tchains = helpers.openHiForest(file_list)

    #select opposite charge generated dimuons within generator acceptance
    df = helpers.defineGenDimuonInfo(df).Filter('gen_dimuon_charge==0 && gen_dimuon_acc')

    #match generated muons to reconstructed muons
    df = df.Define('rec_muon_gen_index', 'rec_muon_match_gen(rec_muon_charge, rec_muon_pt, rec_muon_eta, rec_muon_phi,\
                                                             gen_muon_charge, gen_muon_pt, gen_muon_eta, gen_muon_phi)')
    
    #select gen-matched reconstructed muons
    df = df.Define('rec_muon_gen_mask', 'rec_muon_gen_index>=0')
    df = helpers.addAdditionalMuonInfo(df, 'rec_muon_gen_mask')
    
    #define event selection
    df = helpers.defineEventPreSelection(df)
    
    #define muon selection
    df = helpers.defineMuonSelection(df)

    #define dimuon information
    df = helpers.defineRecDimuonInfo(df)
    
    #save snapshot
    columns = ['gen_dimuon_rapidity', 'rec_dimuon_charge', 'rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_mass', 'rec_dimuon_selection',
               'rec_dimuon_softid', 'rec_dimuon_trigger', 'rec_muon_pt', 'rec_muon_eta', 'rec_muon_trigger', 'event_preselection']
    print(f'Saving skimmed dataframe with columns={columns}')
    df.Snapshot('Events', outname, columns)    
    

    
def openHiForest(file_list,addRecoTrees=True):
    
    """
    opens a HiForest and returns a RDataFrame
    file_list = array of files to process
    """
    
    #we list here all the trees which have information of interest : the first one in the list will be the main one
    #the others will be "friends", i.e. read simultaneously for the same event
    tree_names = [('muonTree/MuonTree', None)]
    if addRecoTrees:
        tree_names+=[('hiEvtAnalyzer/HiTree', 'hi'),
                     ('hltanalysis/HltTree', 'hlt'),
                     ('skimanalysis/HltTree', 'filter')]
    
    tchains = [ROOT.TChain(x) for x,_ in tree_names]
    for i, tchain in enumerate(tchains):
        for f in file_list: 
            tchain.AddFile(f)
        if i==0: continue
        tchains[0].AddFriend(tchain, tree_names[i][1])
    
    return ROOT.RDataFrame(tchains[0]), tchains


def runSelectionWrapper(callback,kwargs,indir,outdir,fileRan=None):
    
    """
    Runs the `callback` method with arguments defined in the `kwargs` dict 
    Two extra arguments will be given to the callback
    * an array of files found in the input directory `indir` 
    * a string with the output file name to save the snapshot
    The signature of the callback is therefore be
    callback(file_list,outname,**kwargs)
    The array of files can be limited to a range by giving an extra argument
    fileRan=(min_counter,max_counter)
    """
    
    #prepare output for single jobs        
    outname=outdir
    if os.path.isfile(outdir) or '.root' in outdir:
        os.system(f'mkdir -p {os.path.dirname(outdir)}')
    else:
        os.system(f'mkdir -p {outdir}')
        if not os.path.isfile(indir):
            outname = f'{outdir}/{os.path.basename(indir)}.root'
    
    #build the list of files
    if os.path.isfile(indir):
        file_list=[indir]
    else:
        file_list = [os.path.join(indir,f) for f in os.listdir(indir) if '.root' in f]
        file_list = [f for f in file_list if os.path.getsize(f)>10000]
        if not fileRan is None:
            file_list = file_list[fileRan[0]:fileRan[1]]
            outname = outname.replace('.root',f'_{fileRan[0]}to{fileRan[1]}.root')
    os.system(f'rm -f {outname}') 
    print(f'Processing a total of {len(file_list)} files')
    print(f'Skimming events from {indir} with output in {outname}')

    #process
    ROOT.ROOT.EnableImplicitMT()
    try:
        callback(file_list=file_list,outname=outname,**kwargs)
    except Exception as e:
        print(e)
    ROOT.ROOT.DisableImplicitMT()
        
    return True


def inspectDistribution(dflist,
                        var,xlabel,bins,ylabel='Events/bin',xscale='linear',yscale='linear'):

    """
    make a simple histogram out of a variable
    dflist is a list of (dataframe, mask, label) to overlay in the plot
    if mask is None, a trivial mask (all events passing) is used
    if documul=True, the cumulative distribution is added
    """
    
    fig,ax=plt.subplots(figsize=(8,8))
    for df,mask,label in dflist:
        if mask is None: mask=np.ones(df.shape[0],dtype=bool)
        ax.hist(df[mask][var],label=label,histtype='step',lw=2,bins=bins)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    ax.grid()
    if len(dflist)>3: 
        ax.legend(bbox_to_anchor=(1.,1.),fontsize=16)
        fig.set_figwidth(12)
    else:
        ax.legend()
    hep.cms.label(loc=1,data=True,rlabel=r'1.52 nb$^{-1}$ ($\sqrt{s_{NN}}=5.02 TeV$)')
    fig.tight_layout()
    plt.show()

    
def computeEfficiency(num,den):
    
    """
    this method wraps the procedure of computing the efficiency eff=num/den 
    with the 68.3% CI computed using a Clopper-Pearson interval based on Beta distribution
    """

    from statsmodels.stats.proportion import proportion_confint
    
    eff = np.divide(num, den, out=np.zeros_like(num, dtype=float), where=den!=0)        
    ci=proportion_confint(num, den, alpha=0.683, method='beta')
    eff_unc=np.array([eff-ci[0],ci[1]-eff])
    return (eff,eff_unc)


def displayEfficiencyPlot(bins,eff,effunc,xlabel,ylabel='Efficiency'):

    """shows the efficiency plot"""
    
    #define the center and the associated "uncertainty" of each bin
    xcen=0.5*(bins[1:]+bins[0:-1])
    xerr=0.5*(bins[1:]-bins[0:-1])
    
    #display as error bar
    fig,ax=plt.subplots(figsize=(8,8))
    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'color':'k','ls':'none'}
    ax.errorbar(xcen,eff,yerr=effunc,xerr=xerr,**ebar_style)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.grid()
    hep.cms.label(loc=0,data=True,rlabel=r'1.52 nb$^{-1}$ ($\sqrt{s_{NN}}=5.02 TeV$)')
    fig.tight_layout()
    plt.show()
    
def showCorrelation(df,x,y,xbins,ybins,xlabel,ylabel):
    
    """make the 2D correlation plot of two variables"""
    
    import matplotlib as mpl
    
    fig, ax = plt.subplots()
    ax.hist2d(df[x],df[y],bins=(xbins,ybins),cmin=0.5,norm=mpl.colors.LogNorm())
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.grid()
    hep.cms.label(loc=0,data=True,rlabel=r'1.52 nb$^{-1}$ ($\sqrt{s_{NN}}=5.02 TeV$)')
    fig.tight_layout()
    plt.show()
