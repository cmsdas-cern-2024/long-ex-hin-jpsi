# Hint : Inspect the ROOT files created under skim
# display the contents of a RDataFrame
# even though RDataFrame has a Display method, pandas provides a more convenient printout

import pandas as pd 
import ROOT 
df=pd.DataFrame(ROOT.RDataFrame('Events','skim/data.root').AsNumpy())
df.head()

# Hint : add more columns to the snapshot
# You can add the following line before the `seldf.Snapshot` line and run again

columns += ['has_dimuon_OS','zdcPlus', 'zdcMinus']


# Hint : add gen level information to the tree

if kwargs['genonly']:
    gendf = helpers.defineGenDimuonInfo(alldf).Filter('gen_dimuon_charge==0')
    gen_outname = outname.replace('.root', '_gen.root')
    gendf.Snapshot('Events', outname, ['gen_dimuon_pt','gen_dimuon_rapidity', 'gen_dimuon_acc'])
    return