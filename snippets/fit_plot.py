import ROOT
import ROOT.RooFit as RF

def shushRooFit() :
    
    ROOT.RooMsgService.instance().setSilentMode(True)
    ROOT.RooMsgService.instance().getStream(0).removeTopic(RF.Minimization)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.Minimization)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.ObjectHandling)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.DataHandling)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.Fitting)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.Plotting)
    ROOT.RooMsgService.instance().getStream(0).removeTopic(RF.InputArguments)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.InputArguments)
    ROOT.RooMsgService.instance().getStream(0).removeTopic(RF.Eval)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.Eval)
    ROOT.RooMsgService.instance().getStream(0).removeTopic(RF.Integration)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.Integration)
    ROOT.RooMsgService.instance().getStream(0).removeTopic(RF.NumIntegration)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(RF.NumIntegration)



def plotFitResults(w,data,model,varname='mass',vartitle='Di-muon invariant mass [GeV]',cname='fit',
                   components=['model_JPsi','model_Psi2S'],component_colors=[8,6],ran=None):
    
    """
    builds a canvas with the display of the result
        * data and model in the first pad
        * fit pull in the second pad
    """

    shushRooFit()
    
    #data and model components
    frame = w.var(varname).frame()
    data.plotOn(frame, MarkerColor='k', MarkerSize=1.2)
    model.plotOn(frame, LineColor=9, LineStyle=1, LineWidth=3)
    
    pullHist = frame.pullHist()
    nFitPar = model.getParameters(data).selectByAttrib("Constant", False).getSize()
    chi2 = frame.chiSquare(nFitPar)

    for c,ci in zip(components,component_colors):
        if ran is None:
            model.plotOn(frame, LineColor=ci, LineStyle=6, LineWidth=2, Components=c)
        else:
            model.plotOn(frame, LineColor=ci, LineStyle=6, LineWidth=2, Components=c, Range=ran, NormRange=ran)            

    #pull
    frame2 = w.var(varname).frame(RF.Title("Residual Distribution"))
    frame2.addPlotable(pullHist,drawOptions="ep")

    #display on a canvas
    c = ROOT.TCanvas(cname+varname, cname+varname, 600, 600)
    c.Divide(1,2)
    p1=c.cd(1)
    p1.SetPad(0.,0.4,1.,1.)
    p1.SetBottomMargin(0.02)
    p1.SetGridx()
    p1.SetGridy()
    frame.Draw();
    frame.GetXaxis().SetTitle('')
    frame.GetXaxis().SetLabelSize(0)
    frame.GetYaxis().SetTitle('Events')
    frame.GetYaxis().SetTitleOffset(1.0)
    frame.GetYaxis().SetLabelSize(0.055)
    frame.GetYaxis().SetTitleSize(0.055)
    
    #printout the fit parameter values in the first pad
    params=model.getParameters(w.var(varname))
    iparam=0
    text = ROOT.TLatex(); text.SetNDC(); text.SetTextSize(0.045); text.SetTextFont(42)
    for p in params:
        text.DrawLatex(0.5,0.85-iparam*0.06,f"{p.GetName()} = {p.getVal():.3f}#pm{p.getError():.3f}")
        iparam+=1
    text.SetTextSize(0.06)
    text.SetTextAlign(ROOT.kHAlignLeft+ROOT.kVAlignCenter)
    text.DrawLatex(0.1,0.94,'#bf{CMS}')
    text.SetTextAlign(ROOT.kHAlignRight+ROOT.kVAlignCenter)
    text.DrawLatex(0.9,0.94,'1.52 nb^{-1} (#sqrt{s_{NN}}=5.02 TeV)')  

    p2=c.cd(2)
    p2.SetPad(0.,0.,1.0,0.4)
    p2.SetTopMargin(0.02)
    p2.SetBottomMargin(0.2)
    p2.SetGridx()
    p2.SetGridy()
    frame2.Draw();
    frame2.GetXaxis().SetTitle(vartitle)
    frame2.GetXaxis().SetTitleSize(0.08)
    frame2.GetXaxis().SetLabelSize(0.08)
    frame2.GetYaxis().SetTitle('Pull')
    frame2.GetYaxis().SetTitleOffset(0.5)
    frame2.GetYaxis().SetTitleSize(0.08)
    frame2.GetYaxis().SetLabelSize(0.08)
    l=ROOT.TLine()
    l.SetLineColor(1)
    l.DrawLine(frame2.GetXaxis().GetXmin(),0,frame2.GetXaxis().GetXmax(),0)

    #print out the chi^2/ndof in the second pad
    text = ROOT.TLatex(); text.SetNDC(); text.SetTextSize(0.08)
    text.DrawLatex(0.12, 0.9, f"\chi^{2}/ndf = {chi2:3.3f}")

    c.Modified()
    c.Update()
    c.Draw()
    
    return c

    p2=c.cd(2)
    p2.SetPad(0.,0.,1.0,0.4)
    p2.SetTopMargin(0.02)
    p2.SetBottomMargin(0.2)
    p2.SetGridx()
    p2.SetGridy()
    frame2.Draw();
    frame2.GetXaxis().SetTitle(vartitle)
    frame2.GetXaxis().SetTitleSize(0.08)
    frame2.GetXaxis().SetLabelSize(0.08)
    frame2.GetYaxis().SetTitle('Pull')
    frame2.GetYaxis().SetTitleOffset(0.5)
    frame2.GetYaxis().SetTitleSize(0.08)
    frame2.GetYaxis().SetLabelSize(0.08)

    #print out the chi^2/ndof in the second pad
    text = ROOT.TLatex(); text.SetNDC(); text.SetTextSize(0.08)
    nFitPar = model.getParameters(data).selectByAttrib("Constant", False).getSize()
    text.DrawLatex(0.12, 0.9, f"\chi^{2}/ndf = {frame.chiSquare(nFitPar):3.3f}")

    c.Modified()
    c.Update()
    c.Draw()
    
    return c