{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a9715a21",
   "metadata": {},
   "source": [
    "#  Coherent J/$\\psi$ production in ultraperipheral PbPb collisions at $\\sqrt{s_{_{\\text{NN}}}}=5.02$ TeV\n",
    "\n",
    "This notebook is part of the Heavy Ion Exercise for the CERN CMSDAS School 2024."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23d7ff94",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import ROOT\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import sys\n",
    "sys.path.append('snippets')\n",
    "import helpers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91cc02db",
   "metadata": {},
   "source": [
    "##  Coherent J/$\\psi$ acceptance\n",
    "\n",
    "A kinematic cut is applied to single muons at generator level to enhance the generation of muons within the CMS detector acceptance and improve the reconstruction efficiency of the simulations.\n",
    "\n",
    "The single muon kinematic seletion is defined as:\n",
    "$p_\\text{T} > 0.7~\\text{GeV/c} , 1.28 < \\left|{\\eta}\\right| < 2.4$\n",
    "\n",
    "To study the impact of the generator-level acceptance selection on the production of coherent J/$\\psi$, we compute the coherent J/$\\psi$ acceptance as:\n",
    "$$\n",
    "  A(\\text{J}/\\psi) = \\frac{N_\\text{Acc}(\\text{J}/\\psi)}{N_\\text{Gen}(\\text{J}/\\psi)}\n",
    "$$\n",
    "\n",
    ", where $N_\\text{Gen}(\\text{J}/\\psi)$ is the number of generated coherent J/$\\psi$ and $N_\\text{Acc}(\\text{J}/\\psi)$ is number of coherent J/$\\psi$ generated within the kinematic acceptance region."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a58ddbf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "#let's load the dataframe for the generator level simulated events\n",
    "gendf=pd.DataFrame(ROOT.RDataFrame('Events','skim/CohJpsiToMuMu_GEN.root').AsNumpy())\n",
    "gendf['gen_dimuon_absrapidity']=np.abs(gendf['gen_dimuon_rapidity'])\n",
    "gendf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78a06012",
   "metadata": {},
   "outputs": [],
   "source": [
    "#define as numerator and denominator the events in different rapidity bins after and before acceptance cuts\n",
    "etabins=np.array([-2.4,-2.1,-1.9,-1.6,1.6,1.9,2.1,2.4])\n",
    "mask_pass=(gendf['gen_dimuon_acc']==True)\n",
    "num = np.histogram(gendf[mask_pass]['gen_dimuon_rapidity'],bins=etabins)[0]\n",
    "den = np.histogram(gendf['gen_dimuon_rapidity'],bins=etabins)[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "206cb503",
   "metadata": {},
   "source": [
    "Computing the acceptance is as trivial as taking num/den.\n",
    "\n",
    "However the associated uncertainties need to be computed using the so-called Clopper-Pearson method \n",
    "(this matters near the edges and in the low statistics regime). You can find more information in the StatsComm twiki page.\n",
    "\n",
    "For this exercise we have coded a helper function but you can also do it using [ROOT::TEfficiency](https://root.cern.ch/doc/master/classTEfficiency.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43e90514",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load -s computeEfficiency snippets/helpers.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7105367d",
   "metadata": {},
   "outputs": [],
   "source": [
    "#compute the acceptance\n",
    "acc,acc_unc = computeEfficiency(num,den)\n",
    "\n",
    "#show the plot\n",
    "helpers.displayEfficiencyPlot(etabins,acc,acc_unc,xlabel='Di-muon rapidity',ylabel='Acceptance')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0043609",
   "metadata": {},
   "source": [
    "##  Coherent J/$\\psi$ efficiency\n",
    "\n",
    "The impact of the event selection, muon reconstruction and trigger on the production coherent J/$\\psi$ is assesed by evaluating the efficiency in MC simulations.\n",
    "\n",
    "The coherent J/$\\psi$ efficiency is defined as:\n",
    "$$\n",
    "  \\epsilon(\\text{J}/\\psi) = \\frac{N_\\text{Evt. Sel., ID, trigger}(\\text{J}/\\psi)}{N_\\text{Acc}(\\text{J}/\\psi)}\n",
    "$$\n",
    "\n",
    ", where $N_\\text{Acc}(\\text{J}/\\psi)$ is number of coherent J/$\\psi$ generated within the kinematic acceptance region and $N_\\text{Evt. Sel., ID, trigger}(\\text{J}/\\psi)$ is the number of reconstructed coherent J/$\\psi$ passing the event selection, muon identification and trigger."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec6d3ade",
   "metadata": {},
   "outputs": [],
   "source": [
    "#load the variables of interest from a signal simulation\n",
    "columns=['gen_dimuon_rapidity','rec_dimuon_pt','rec_dimuon_rapidity','rec_dimuon_charge',\n",
    "        'rec_dimuon_selection','rec_dimuon_softid', 'rec_dimuon_trigger', 'event_preselection']\n",
    "df=pd.DataFrame(ROOT.RDataFrame('Events','skim/CohJpsiToMuMu.root').AsNumpy(columns=columns))\n",
    "df['gen_dimuon_absrapidity']=np.abs(df['gen_dimuon_rapidity'])\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41656ca0",
   "metadata": {},
   "outputs": [],
   "source": [
    "etabins=np.array([-2.4,-2.1,-1.9,-1.6,1.6,1.9,2.1,2.4])\n",
    "\n",
    "#filter out signal-like events\n",
    "mask_pass=(df['rec_dimuon_pt']<0.2) & (df['rec_dimuon_charge']==0) & (df['rec_dimuon_selection'])\n",
    "mask_pass &= df['rec_dimuon_softid'] & df['rec_dimuon_trigger'] & df['event_preselection']\n",
    "\n",
    "#compute the efficiency\n",
    "num = np.histogram(df[mask_pass]['gen_dimuon_rapidity'],bins=etabins)[0]\n",
    "den = np.histogram(df['gen_dimuon_rapidity'],bins=etabins)[0]\n",
    "eff,eff_unc = helpers.computeEfficiency(num,den)\n",
    "\n",
    "#show the plot\n",
    "helpers.displayEfficiencyPlot(etabins,eff,eff_unc,xlabel='Di-muon rapidity',ylabel='Efficiency')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5ec4b20",
   "metadata": {},
   "source": [
    "##  Coherent J/$\\psi$ efficiency corrected with scale factors from data\n",
    "\n",
    "There are numerous reasons for differences in the simulated efficiency and the actual detector efficiency. As such data-based methods must be employed to measure these differences and correct the expectations from pure simulation.\n",
    "\n",
    "The simulated coherent J/$\\psi$ efficiency is corrected using single muon efficiency ratios derived with the Tag-and-Probe (TnP) technique.\n",
    "\n",
    "The TnP technique is a data-driven method that allows to derive the single muon efficiency in both data and simulation. We'll get a flavour of it in a forthcoming notebook. In this case however we are assuming the efficiencies measured by another group and applying them to our analysis.\n",
    "\n",
    "By taking the ratio between the muon efficiency in data over the one in simulation, one can produce scale factors that are used to correct the simulated efficiencies. For more information check [link](https://cms-opendata-workshop.github.io/workshop-lesson-tagandprobe/aio/index.html).\n",
    "\n",
    "In this case, the correction is applied to the simulated coherent J/$\\psi$ efficiency as:\n",
    "\n",
    "$$\n",
    "  \\epsilon_\\text{TnP}(\\text{J}/\\psi) = \\epsilon(\\text{J}/\\psi) \\times \\frac{\\epsilon^\\text{data}_\\text{TnP}(\\mu^{+})}{\\epsilon^\\text{MC}_\\text{TnP}(\\mu^{+})} \\times \\frac{\\epsilon^\\text{data}_\\text{TnP}(\\mu^{-})}{\\epsilon^\\text{MC}_\\text{TnP}(\\mu^{-})}\n",
    "  =  \\epsilon(\\text{J}/\\psi) \\times SF(\\mu^+) \\times SF(\\mu^-)\n",
    "$$\n",
    "\n",
    "\n",
    ", where $\\epsilon^\\text{data}_\\text{TnP}(\\mu^{\\pm})$ ($\\epsilon^\\text{MC}_\\text{TnP}(\\mu^{\\pm})$) are the single muon efficiencies derived from data (simulation) using the TnP method. To apply them, one effectively weights the numerator of the efficiency by the TnP scale factors (SF)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc62d239",
   "metadata": {},
   "source": [
    "We start by adding the scale factors to the data.\n",
    "\n",
    "In this cell we'll step back a little and apply the scale factors using RDataFrame.\n",
    "The reason is that the format in which they have been provided is based on a C header so it's more convenient to use them in ROOT."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f626319",
   "metadata": {},
   "outputs": [],
   "source": [
    "ROOT.gInterpreter.Declare('#include \"snippets/helpers.h\"')\n",
    "ROOT.ROOT.EnableImplicitMT()\n",
    "\n",
    "#filter only the events passing the final selection\n",
    "rdf = ROOT.RDataFrame('Events', 'skim/CohJpsiToMuMu.root')\n",
    "rdf = rdf.Filter('rec_dimuon_pt<0.2')\\\n",
    "         .Filter('rec_dimuon_charge==0 && rec_dimuon_selection')\\\n",
    "         .Filter('rec_dimuon_softid && rec_dimuon_trigger && event_preselection') \\\n",
    "         .Define('tnp_weight_softid', 'tnp_weight_softid(rec_muon_pt, rec_muon_eta)')\\\n",
    "         .Define('tnp_weight_trigger', 'tnp_weight_trigger(rec_muon_pt, rec_muon_eta)')\\\n",
    "         .Define('dimuon_tnp_weight_pid', 'tnp_weight_softid.size()>1 ? (tnp_weight_softid[0] * tnp_weight_softid[1]) : 1')\\\n",
    "         .Define('dimuon_tnp_weight_trg', 'tnp_dimuon_weight_trigger(tnp_weight_trigger, rec_muon_trigger)')\\\n",
    "         .Define('dimuon_tnp_weight', 'dimuon_tnp_weight_pid * dimuon_tnp_weight_trg')\n",
    "\n",
    "#add the tag and probe weight to the columns of interst\n",
    "columns=['gen_dimuon_rapidity','rec_dimuon_pt','rec_dimuon_rapidity','rec_dimuon_charge',\n",
    "        'rec_dimuon_selection','rec_dimuon_softid', 'rec_dimuon_trigger', 'event_preselection',\n",
    "         'dimuon_tnp_weight']\n",
    "df_num=pd.DataFrame(rdf.AsNumpy(columns=columns))\n",
    "ROOT.ROOT.DisableImplicitMT()\n",
    "\n",
    "#preview the new data\n",
    "df_num['gen_dimuon_absrapidity']=np.abs(df_num['gen_dimuon_rapidity'])\n",
    "df_num.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04b116cc",
   "metadata": {},
   "source": [
    "Now we plot the efficiency corrected by the scale factors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4922f33",
   "metadata": {},
   "outputs": [],
   "source": [
    "#compute the efficiency, now applying the scale factors as weights to fill the histogram of the numerator\n",
    "sfnum = np.histogram(df_num['gen_dimuon_rapidity'], weights=df_num['dimuon_tnp_weight'], bins=etabins)[0]\n",
    "sfeff,sfeff_unc = helpers.computeEfficiency(sfnum,den)\n",
    "\n",
    "#show the plot\n",
    "helpers.displayEfficiencyPlot(etabins,sfeff,sfeff_unc,xlabel='Di-muon rapidity',ylabel='SF x Efficiency')\n",
    "\n",
    "#print out the efficiency-corrected values\n",
    "for i in range(len(etabins)-1):\n",
    "    avg_unc=0.5*(sfeff_unc[0][i]+sfeff_unc[1][i])\n",
    "    print(f'Corrected efficiency for bin: {etabins[i]} <= |y| < {etabins[i+1]} = {sfeff[i]:3.4f} +/- {avg_unc:3.4f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6477b35c",
   "metadata": {},
   "source": [
    "## Next steps\n",
    "\n",
    "* compute the acceptance as function of |y| and print out the values obtained\n",
    "   * if you need help load the following hint: `%load -r 1-17 hints/hints_step4.py` in a cell\n",
    "* compute the efficiency as function of |y| and print out the values obtained\n",
    "   * if you need help load the following hint: `%load -r 20-31 hints/hints_step4.py` in a cell\n",
    "* similar to the previous but after applying the scale factors\n",
    "   * if you need help the previous hint is still valid :)\n",
    "* how different were the efficiency scale factors after correcting with the scale factors?\n",
    "* show in a plot the combined Acceptance x SF x efficiency\n",
    "   * if you need help load the following hint: `%load -r 34-41 hints/hints_step4.py` in a cell\n",
    "   \n",
    "Once all is done move to the next notebook `Step5a-FittingYields.ipynb`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c5d079c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
