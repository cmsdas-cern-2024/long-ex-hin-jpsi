{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "98d56772",
   "metadata": {},
   "source": [
    "#  Coherent J/$\\psi$ production in ultraperipheral PbPb collisions at $\\sqrt{s_{_{\\text{NN}}}}=5.02$ TeV\n",
    "\n",
    "## Introduction\n",
    "\n",
    "This notebook is part of the Heavy Ion Exercise for the CERN CMSDAS School 2024.\n",
    "\n",
    "In this exercise we will explore the production of the J/$\\psi$ vector meson via photon-nuclear intereations.\n",
    "The production cross section of this process is a direct probe of the nuclear gluon density function. In ultra peripheral collisions (UPCs) the two ions \"miss\" each other but in the crossing they generate strong electromagnetic fields corresponding to $B\\sim \\mathcal{O}(10^{16})~T$. In this process both nuclei can serve either as photon-emitter projectile or as target as shown in Figure 1 below.\n",
    "\n",
    "![Proceses of interest](http://cms-results.web.cern.ch/cms-results/public-results/publications/HIN-22-002/CMS-HIN-22-002_Figure_001.png) \n",
    "\n",
    "_Figure 1_ - A pictorial representation of the J/$\\Psi$ photoproduction process in PbPb at leading-order QCD, showing the origin of the two-way ambiguity. The blue wavy and black helical lines represent emitted photons and gluons, respectively. Taken from [HIN-22-002](http://cms-results.web.cern.ch/cms-results/public-results/publications/HIN-22-002/).\n",
    "\n",
    "The two contributions picture above correspond to two different physics signatures:\n",
    "* _incoherent production_: the photon has high energy and therefore small $\\lambda$ and couples partially to the nucleus. As a consequence the nucleus tends to breaks-up and the vector meson acquires a small boost with $p_T\\sim 500~MeV$\n",
    "* _coherent production_: in this case $\\lambda = \\hbar/k > 2 R$ and the photon couples to the nucleus as a whole. Consequently the vector meson will be produced close to rest ($p_T\\sim 50~MeV$) and the nucleus will stay intact.\n",
    "\n",
    "Coherent production is particularly interesting because it probes directly the small-x regime of the nuclear gluon PDF. This is hardly achievable in any other system. In this extreme regime gluon splittings dominate and tend to increase rapidly. Therefore we have a unique opportunity of testing of how unitarity is restored, i.e. of measuring which QCD mechanism takes over gluon splitting.\n",
    "\n",
    "Experimentally we will identify the coherent production by interpreting the $p_\\text{T}$ distribution of the $J/\\psi$ candidates and we'll furthermore separate it by making use of the Zero Degree Calorimeter (ZDC) of CMS. CMS ZDCs are two identical forward calorimeters located between the two LHC beam pipes at a distance of approximately 140 m from the CMS interaction point along the beamline, on each side. The figure below illustrates how one of the arms is composed.\n",
    "\n",
    "![ZDC](https://inspirehep.net/files/0983b0d673d1c5be43a92c3dff7c049e)\n",
    "\n",
    "_Figure 2_ - Schematic side-view of the CMS ZDC. The calorimeter is placed at an angle $\\theta<0.5$ mrad, corresponding to $\\left|\\eta\\right|>8.3$ inside a special detector slot in the neutral particle absorber (TAN), which is used to protect the first LHC superconducting quadrupole magnet from radiation. The sampling sections interleave W and quartz fibers with 5 horizontal electromagnetic divisions and 4 longitudinal hadronic divisions. The electromagnetic (hadronic) section totals 19 $X_0$ (5.6$\\lambda$). Figure sourced from [JINST 16 (2021) 05, P05008](https://arxiv.org/pdf/2102.06640.pdf).\n",
    "\n",
    "From the spectrum of energy deposited in the ZDC we'll be able to perform a counting of the forward neutrons which haven't been deflected by the experiment and LHC magnets.\n",
    "\n",
    "\n",
    "## Data preparation\n",
    "\n",
    "In this notebook we'll prepare the data for our analysis.\n",
    "\n",
    "The events have already been pre-processed and stored in the custom format used by the heavy-ion group in CMS in Run 1 and Run2, called `HeavyIonForest`. In the original file the information is scattered among different structures ([TTrees](https://root.cern.ch/root/htmldoc/guides/users-guide/Trees.html)).\n",
    "\n",
    "In our exercise we'll make use of [RDataFrames](https://root.cern/manual/data_frame/) to analyse the events. RDataFrame is a high-level interface to data stored in TTree or other formats and offers multithreading and low-level optimizations to explore data. The calculations are expressed in terms of a functional chain of actions and transformations forming a computational graph as in the example below.\n",
    "\n",
    "![RDataFrame graph](https://root.cern/doc/master/RDF_Graph.png)\n",
    "\n",
    "_Figure 3_ - A RDataFrame graph composed of two branches, one starting with a filter and one with a define. The end point of a branch is always an action. \n",
    "\n",
    "Each declaration creates either a new node or a new column in the data format. The actual execution is only made at the end or by requesting explicitly the code to run.\n",
    "\n",
    "## Event selection\n",
    "\n",
    "Events are selected using the following criterias:\n",
    "\n",
    "* **Single muon trigger (HLT_HIUPC_SingleMuOpen_NotMBHF2AND_v1)**: requires at least one hadronic forward (HF, $2.9 < \\left|\\eta\\right| < 5.2$) calorimeter to have no signal above the noise threshold and at least one muon reconstructed by the Level 1 trigger system in the muon chambers.\n",
    "* **Primary vertex filter (pprimaryVertexFilter)**: requires at least one primary vertex formed by at least two tracks and to be located within 25 cm (2 cm) along the longitudinal (transverse) direction from the CMS detector center.\n",
    "* **Cluster shape compatibility (pclusterCompatibilityFilter)**: requires the shapes of the clusters in the pixel detector to be compatible with the shapes expected from a heavy ion collision to suppress beam scraping events.\n",
    "* **UPC HF selection (hfFilter)**: requires that all HF towers have an energy below the HF noise thresholds (7.3 GeV in forward side and 7.6 GeV in backward side), to suppress inclusive hadronic interactions. These two noise thresholds are determined from empty bunch crossing, which have no beam passing through the interaction point and thus no collisions taking place.\n",
    "* **Two high purity tracks (twoHighPurityTracks)**: requires the presence of only two tracks passing the high purity quality criteria, as expected for the J/$\\psi\\rightarrow\\mu^{+}\\mu^{-}$ decay process.\n",
    "* **Runs with ZDC (runsWithZDC)**: selects run numbers above 326776 in which the ZDC was functional.\n",
    "\n",
    "![Event display](https://cds.cern.ch/record/2648517/files/HI-UPC_v0.png?subformat=icon-640)\n",
    "\n",
    "## Muon selection\n",
    "\n",
    "In addition, muon candidates are selected by applying the [Soft identification (ID)](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideMuonIdRun2#Soft_Muon) criteria optimized by the muon POG:\n",
    "* **isTracker and oneStationTight**: muon whose inner track is matched to a hit in the muon chambers and at least one muon station within a tight X-Y window.\n",
    "* **isHighPurity, nPixelLayers>0 and nTrackerLayers>5**: muon whose inner track pass the high purity quality criteria and is measured in at least one pixel layer and six tracker layers.\n",
    "* **|dXY|<0.3 and |dZ|<20**: ensures that the muon is produced within 20 cm (0.3 cm) along the longitudinal (transverse) direction from the primary vertex. Removes displaced and cosmic muons.\n",
    "\n",
    "![Muon](https://cms.cern/sites/default/files/inline-images/MuStations.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8bdbfb4",
   "metadata": {},
   "source": [
    "The following cell imports some utilities used throughout the notebook. \n",
    "\n",
    "The `helpers.py` file contains several methods which factorize some common operations for handling the data (e.g. gather files of interest to analyse, common open file operations, etc.). Please take some time to glance over it. You can also create a new cell and list the methods with `dir(helpers)` and check their properties with e.g. `addAdditionalMuonInfo?`.\n",
    "\n",
    "Besides the python imports the cell below declares a header `helpers.h` file which contains some functions used to process the data. These functions will be pre-compiled through ROOT and will be available when using the RDataFrame framework. It is suggested to take a look into the `helpers.h` file for further details. This is a useful way of pre-compiling complicated operations which would become too lengthy to describe in a single line. An example is a function which loops over a collection of objects and finds if there is some overlap with respect to some reference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "dd869008",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Welcome to JupyROOT 6.30/04\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "    \n",
    "import os\n",
    "import sys\n",
    "import ROOT\n",
    "sys.path.append('snippets')\n",
    "import helpers\n",
    "\n",
    "#use helper script to define more complicated selection functions (see snippets/helpers.h for details)\n",
    "ROOT.gInterpreter.Declare('#include \"snippets/helpers.h\"')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63781f1a",
   "metadata": {},
   "source": [
    "The following cell loads a method to pre-select the events and save the information of interest to analyze later. You'll notice that the computational graph is something like the following\n",
    "\n",
    "* Data &rarr; \n",
    "   * Lumi : has ZDC and single muon trigger active?\n",
    "   * Empty bunch: has ZDC and event triggered from an empty bunch?\n",
    "   * Pre-selection: pass pre-selection requirements?\n",
    "* Pre-selection &rarr;\n",
    "   * ZDC-analysis: Has opposite-sign (OS) di-muon?\n",
    "   * Signal region: Has a J/Psi candidate?\n",
    "\n",
    "This pre-selection of the events only needs to be done once at start, then one can use the skimmed files directly for analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "c2ae7190",
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -s runJPsiSelection snippets/helpers.py\n",
    "def runJPsiSelection(file_list,\n",
    "                     outname,\n",
    "                     mu_attrs,**kwargs):\n",
    "    \n",
    "    \"\"\"\n",
    "    runs a selection of events and saves a skimmed RDataFrame\n",
    "    file_list = array of files to process\n",
    "    outname = output name of the skimmed file\n",
    "    mu_attrs = array of muon attributes to save in summary\n",
    "    \"\"\"\n",
    "    \n",
    "    #start a RDataFrame to analyse the events\n",
    "    alldf, tchains = helpers.openHiForest(file_list)\n",
    "    \n",
    "    #define event selection\n",
    "    df = helpers.defineEventPreSelection(alldf)\n",
    "\n",
    "    #define a pre-selection on empty bunches to monitor ZDC noise\n",
    "    emptydf = df.Filter('runsWithZDC && clusterCompatibility && hfFilter && hlt.HLT_HIL1NotBptxOR_v1>0')\n",
    "    emptydf = helpers.addZDCInfo(emptydf)\n",
    "    \n",
    "    #define pre-selection branch for all events which triggered and have the ZDC available\n",
    "    lumidf = df.Filter('runsWithZDC && fireTrigger')\n",
    "\n",
    "    #add the ZDC info \n",
    "    lumidf = helpers.addZDCInfo(lumidf)\n",
    "    \n",
    "    #define a branch all events passing pre-selection with two high purity tracks\n",
    "    preseldf = lumidf.Filter('event_preselection')\n",
    "    \n",
    "    #add a flag if the event has OS muons\n",
    "    preseldf =  preseldf.Define('has_dimuon_OS', 'Sum(rec_dimuon_charge==0)>0')\n",
    "    \n",
    "    #define a ZDC selection branch for events with OS di-muons passing the pre-selection\n",
    "    #the muons do not necessarily need to be high purity at this stage\n",
    "    zdcdf = preseldf.Filter('has_dimuon_OS') \n",
    "    \n",
    "    #define the di-muon selection branch with two muons, at least one being a good muon \n",
    "    #the di-muon system will also be required to be in the vicinty of the J/Psi mass\n",
    "    #additional selection variables are added by the methods being called\n",
    "    seldf = preseldf.Define('rec_muon_maskHP', 'rec_muon_track_isHighPurity>0')\n",
    "    seldf = helpers.addAdditionalMuonInfo(seldf, 'rec_muon_maskHP')\n",
    "    seldf = helpers.defineMuonSelection(seldf)\n",
    "    seldf = seldf.Filter('nmu==2 && ngood_mu>0')\n",
    "    seldf = helpers.defineRecDimuonInfo(seldf)\n",
    "    seldf = seldf.Filter('rec_dimuon_selection && rec_dimuon_trigger')\n",
    "    \n",
    "    #\n",
    "    # with all the nodes defined it's time to execute them - this will be done by calling\n",
    "    # the snapshot method which will run the selections and store the resulting events in files\n",
    "    # we start by calling the most complicated branch which is the `seldf` one\n",
    "    #\n",
    "    \n",
    "    #define collections for the selected muon and add to output before calling Snapshot\n",
    "    columns = []\n",
    "    for i in range(2):\n",
    "        for attr in mu_attrs:\n",
    "            seldf = seldf.Define(f'rec_muon{i+1}_{attr}', f'rec_muon_{attr}[{i}]') \n",
    "            columns.append(f'rec_muon{i+1}_{attr}')\n",
    "    columns += ['rec_dimuon_mass', 'rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_charge', 'rec_dimuon_softid', 'zdcMinus', 'zdcPlus']\n",
    "    print(f'Saving skimmed dataframe with columns={columns}')\n",
    "    seldf.Snapshot('Events', outname, columns)\n",
    "\n",
    "    #snapshot for luminosity computations\n",
    "    lumi_outname = outname.replace('.root', '_lumi.root')\n",
    "    print(f'Saving skimmed dataframe for computing luminosity in {lumi_outname}')\n",
    "    #lumidf.Snapshot('Lumi', lumi_outname, ['run', 'lumi'])\n",
    "  \n",
    "    #save snapshot for ZDC analysis\n",
    "    zdc_fname = outname.replace('.root', '_zdc.root')\n",
    "    print(f'Saving skimmed dataframe for analyzing ZDC spectra in {zdc_fname}')\n",
    "    #zdcdf.Snapshot('ZDC', zdc_fname, ['zdcPlus', 'zdcMinus'])\n",
    "\n",
    "    #save snapshot for ZDC noise control\n",
    "    empty_fname = outname.replace('.root','_emptybx.root')\n",
    "    print(f'Saving skimmed dataframe for analyzing ZDC noise (empty BXs) in {empty_fname}')\n",
    "    #emptydf.Snapshot('ZDC', empty_fname, ['zdcPlus', 'zdcMinus'])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "951ff31f",
   "metadata": {},
   "source": [
    "We now run the previous method on all the available data. This takes a bit (~8-9 min wall time) but fortunately only needs to be done once. When you start the notebook choose as many CPU's as possibly available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0d73a74b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Processing a total of 879 files\n",
      "Skimming events from /eos/user/c/cmsdas/2024/long-ex-hin/data with output in skim/data.root\n",
      "Saving skimmed dataframe with columns=['rec_muon1_pt', 'rec_muon1_eta', 'rec_muon1_phi', 'rec_muon1_charge', 'rec_muon1_softid', 'rec_muon1_trigger', 'rec_muon2_pt', 'rec_muon2_eta', 'rec_muon2_phi', 'rec_muon2_charge', 'rec_muon2_softid', 'rec_muon2_trigger', 'rec_dimuon_mass', 'rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_charge', 'rec_dimuon_softid', 'zdcMinus', 'zdcPlus']\n",
      "Template method resolution failed:\n",
      "  none of the 3 overloaded methods succeeded. Full details:\n",
      "  ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager,void> > ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter,void>::Snapshot(basic_string_view<char,char_traits<char> > treename, basic_string_view<char,char_traits<char> > filename, initializer_list<string> columnList, const ROOT::RDF::RSnapshotOptions& options = ROOT::RDF::RSnapshotOptions()) =>\n",
      "    TypeError: could not convert argument 3\n",
      "  ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager,void> > ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter,void>::Snapshot(basic_string_view<char,char_traits<char> > treename, basic_string_view<char,char_traits<char> > filename, const vector<string>& columnList, const ROOT::RDF::RSnapshotOptions& options = ROOT::RDF::RSnapshotOptions()) =>\n",
      "    KeyboardInterrupt: \n",
      "  ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager,void> > ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter,void>::Snapshot(basic_string_view<char,char_traits<char> > treename, basic_string_view<char,char_traits<char> > filename, basic_string_view<char,char_traits<char> > columnNameRegexp = \"\", const ROOT::RDF::RSnapshotOptions& options = ROOT::RDF::RSnapshotOptions()) =>\n",
      "    TypeError: could not convert argument 3\n",
      "  ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager,void> > ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter,void>::Snapshot(basic_string_view<char,char_traits<char> > treename, basic_string_view<char,char_traits<char> > filename, initializer_list<string> columnList, const ROOT::RDF::RSnapshotOptions& options = ROOT::RDF::RSnapshotOptions()) =>\n",
      "    TypeError: could not convert argument 3\n",
      "Processing a total of 1 files\n",
      "Skimming events from /eos/user/c/cmsdas/2024/long-ex-hin/dataset/HiForestAOD_HIEmptyBX.root with output in skim_emptybx/data.root\n",
      "Saving skimmed dataframe with columns=['rec_muon1_pt', 'rec_muon1_eta', 'rec_muon1_phi', 'rec_muon1_charge', 'rec_muon1_softid', 'rec_muon1_trigger', 'rec_muon2_pt', 'rec_muon2_eta', 'rec_muon2_phi', 'rec_muon2_charge', 'rec_muon2_softid', 'rec_muon2_trigger', 'rec_dimuon_mass', 'rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_charge', 'rec_dimuon_softid', 'zdcMinus', 'zdcPlus']\n",
      "Saving skimmed dataframe for computing luminosity in skim_emptybx/data_lumi.root\n",
      "Saving skimmed dataframe for analyzing ZDC spectra in skim_emptybx/data_zdc.root\n",
      "Saving skimmed dataframe for analyzing ZDC noise (empty BXs) in skim_emptybx/data_emptybx.root\n",
      "CPU times: user 2min 7s, sys: 33.5 s, total: 2min 40s\n",
      "Wall time: 5min 38s\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning in <RTaskArenaWrapper>: CPU Bandwith Control Active. Proceeding with 2 threads accordingly\n",
      "Warning in <RTaskArenaWrapper>: CPU Bandwith Control Active. Proceeding with 2 threads accordingly\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "    \n",
    "#the method runSelectionWrapper will take care of building the appropriate file list from the directory\n",
    "#and delegate the analysis to the method above\n",
    "kwargs={'mu_attrs': ['pt', 'eta', 'phi', 'charge', 'softid', 'trigger']}\n",
    "helpers.runSelectionWrapper(callback=runJPsiSelection,\n",
    "                            kwargs=kwargs,\n",
    "                            indir='/eos/user/c/cmsdas/2024/long-ex-hin/data',\n",
    "                            outdir='skim',\n",
    "                            fileRan=None)\n",
    "\n",
    "#repeat the same for empty BX\n",
    "#helpers.runSelectionWrapper(callback=runJPsiSelection,\n",
    "                            kwargs=kwargs,\n",
    "                            indir='/eos/user/c/cmsdas/2024/long-ex-hin/dataset/HiForestAOD_HIEmptyBX.root',\n",
    "                            outdir='skim_emptybx/data.root',\n",
    "                            fileRan=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "722678c5",
   "metadata": {},
   "source": [
    "## Next steps\n",
    "\n",
    "* Two simple exercises:\n",
    "   * Inspect the ROOT files created under skim (If help needed you can run `%load -r 1-8 hints/hints_step1.py` on a cell to get the solution)\n",
    "   * Add the variables `has_dimuon_OS`,`zdcPlus`, `zdcMinus` to the columns of the final analysis dataframe (If help needed you can run `%load -r 10-13 hints/hints_step1.py` on a cell to get the solution)\n",
    "\n",
    "* For the **audacious**:\n",
    "   * Implement a new branch to inspect generator level J/Psi before any selection and store in a different file in case it's not data. Use it to process the signal `/eos/user/c/cmsdas/2024/long-ex-hin/simulation/HiForestAOD_CohJpsiToMuMu_GENONLY.root` and inspect the output. (Hint check `helpers.py` for a method related to MC information. If help needed you can run `%load -r 16-22 hints/hints_step1.py` on a cell). We'll reprise this in the step 3 notebook\n",
    "   * Prepare a skim where the J/$\\psi$  invariant mass requirement is not used. Use it later to plot the di-muon mass and inspect possible other structures (Hint check `helpers.py` for the place where the J/$\\psi$ requirement is applied).\n",
    "   \n",
    "Once you're done, it's time to inspect the data from the skimmed file using `Step2-InspectSkimmedData.ipynb`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eab3fde1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
